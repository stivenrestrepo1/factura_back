from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter

from app.validaciones.validations_view import BaseViewSet
from app.usuarios.empleado.model import User
from app.usuarios.empleado.serializer import UserSerializer, UpdatedPasswordSerializer 
from app.usuarios.empleado.filter import UserFilter

class UserViewSet(BaseViewSet):
    '''
    ViewSet para usuarios
    '''

    app_code = 'user'
    permission_code = 'user'
    queryset = User.objects.all().prefetch_related('groups')
    serializer_class = UserSerializer
    filter_class = UserFilter
    search_fields = ('username', 'first_name', 'last_name', 'email')
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    def create(self, request):

        self.serializer_class = UserSerializer

        return super().create(request)

class UpdatedPasswordViewSet(BaseViewSet):
    '''
    ViewSet para actualizar contraseña
    '''

    app_code = 'user'
    permission_code = 'user'
    queryset = User.objects.all()
    serializer_class = UpdatedPasswordSerializer

    def create(self, request):

        self.serializer_class = UpdatedPasswordSerializer

        return super().create(request)