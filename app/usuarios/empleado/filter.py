from django_filters import rest_framework as filters
from .model import User


class UserFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = User
        fields = {
            'id': ['in'],
            'identification': ['in'],
            'first_name': ['icontains', 'exact'],
            'last_name': ['icontains', 'exact'],
            'email': ['icontains', 'exact'],
            'type_user': ['exact'],
            'groups': ['exact'],
        }