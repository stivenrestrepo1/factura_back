# import requests
from django.conf import settings
from rest_framework import status
from rest_framework.response import Response
from django.contrib.auth.models import Group
from rest_framework import serializers

from app.usuarios.empleado import User, type_user_list


class UserSerializer(serializers.ModelSerializer):
    '''
    User serializer
    '''
    identification = serializers.CharField(label='Identificacion', required=True, allow_null=False, allow_blank=False)
    birth_date = serializers.DateField(label='Fecha de nacimiento', required=False,  allow_null=True)
    phone = serializers.CharField(label='Teléfono', required=False, allow_null=True, allow_blank=True)
    email = serializers.EmailField(label='Correo electrónico', required=False, allow_null=True, allow_blank=True)
    first_name = serializers.CharField(label='Nombre(s)', required=True, allow_null=False, allow_blank=False)
    last_name = serializers.CharField(label='Apellido(s)', required=True, allow_null=False, allow_blank=False)
    password = serializers.CharField(label='Contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirme contraseña', required=False, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    type_user = serializers.ChoiceField(label='Tipo de usuario', choices=type_user_list, required=True, write_only=False)

    """Validacion de los campos que viene por post"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'data' in kwargs:
            if 'partial' in kwargs:
                self.fields['first_name'].required = False
                self.fields['first_name'].allow_blank = True
                self.fields['last_name'].required = False
                self.fields['last_name'].allow_blank = True
                self.fields['email'].required = False
                self.fields['email'].allow_blank = True
                self.fields['password'].required = False
                self.fields['password'].allow_blank = True
                self.fields['confirm_password'].required = False
                self.fields['confirm_password'].allow_blank = True
            
    
    def update(self, instance, validated_data):

        instance.identification = validated_data.get('identification', instance.identification)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.is_superuser = validated_data.get('is_superuser', instance.is_superuser)
        instance.type_user = validated_data.get('type_user', instance.type_user)
        instance.groups.set(validated_data['groups'])
        
        instance.save()
        
        return instance
    
    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        try:
            validated_data.pop('confirm_password')
        except:
            pass

        user = User.objects.create_user(**validated_data)
        user.groups.set(groups_data)    
        return user
    
    def retrive(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def destroy(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response('detail:error', status=status.HTTP_204_NO_CONTENT) 
    

    class Meta:
        '''
        Meta tags
        '''

        model = User
        fields = (
            'identification',
            'type_user',
            'email',
            'birth_date',
            'phone',
            'first_name',
            'last_name',
            'is_superuser',
            'groups',
            'password',
            'confirm_password',
        )

class UpdatedPasswordSerializer(serializers.ModelSerializer):

    last_password = serializers.CharField(label='Contraseña actual', required=True, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    password = serializers.CharField(label='Contraseña nueva', required=True, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)
    confirm_password = serializers.CharField(label='Confirme contraseña', required=True, allow_null=True, allow_blank=True, style={'input_type': 'password'}, write_only=True)

    def update(self, instance, validated_data):

        # valida que la contraseña ingresada sea igual a la que esta en BD
        validation = instance.check_password(validated_data.get('last_password'))

        try:
            if validation == True:

                if validated_data.get('password') == validated_data.get('confirm_password'):


                    instance.password = validated_data.get('password', instance.password)
                    instance.set_password(instance.password)
                    instance.save()
                                   
                    return instance

                else:
                    raise serializers.ValidationError({'password': 'Las credenciales proporsionadas no son validas.'})
            else:
                raise serializers.ValidationError({'last_password': 'No concuerda la contraseña actual - Las credenciales proporsionadas no son validas.'})
        except Exception as e:
            raise e


    class Meta():
        model = User
        fields = (
            'id',
            'last_password',
            'password',
            'confirm_password'
        )
        