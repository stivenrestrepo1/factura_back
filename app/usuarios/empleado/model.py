from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

admin = 'Administrador'
employed = 'Empleado'

type_user_list = [
    (admin, 'Administrador'),
    (employed, 'Empleado')

]

"""se importa en modelo de usuario de django y se crea la funcion para el super user y el user"""
class UserManager(BaseUserManager):

    def create_user(self, identification, email, type_user, password=None, **extra_fields):

        user = self.model(
            identification=identification,
            email=self.normalize_email(email),
            type_user=type_user,
            **extra_fields
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, identification, email, password, type_user):
       
        user = self.create_user(
            email=email,
            identification=identification,
            password=password,
            type_user=type_user,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class User(AbstractUser):
    '''
    Campos adicionales para el modelo de usuarios
    '''
    type_user = models.CharField('Tipo de usuario', choices=type_user_list, max_length=25)

    identification = models.CharField(max_length=15, null=False, blank=False, unique=True, verbose_name = "identificación")
     
    first_name = models.CharField(max_length=60, null=False, blank=False, verbose_name = "nombre")

    last_name = models.CharField(max_length=70, null=False, blank=False, verbose_name = "apellido")
    
    birth_date = models.DateField(null=True, blank=True, verbose_name = "fecha de nacimiento")
    
    phone = models.CharField(max_length=15, null=True, verbose_name = "telefono")

    username = None

    USERNAME_FIELD = 'identification'

    objects = UserManager()

    REQUIRED_FIELDS = ['email', 'identification', 'first_name', 'last_name', 'type_user']

    class Meta:
        '''
        Meta tags
        '''
        verbose_name_plural = 'Usuarios'
        verbose_name = 'Usuario'
        # Permisos por defecto desactivados
        default_permissions = ()
        # Se crear los propios permisos y de esta forma tenerlos en español
        permissions = (
            # Usuarios
            ('add_user', 'Crear usuarios'),
            ('change_user', 'Actualizar un usuario'),
            ('list_user', 'Consultar usuarios'),
            ('retrieve_user', 'Consultar un usuario')
        )